msgid ""
msgstr ""
"Project-Id-Version: wdps\n"
"POT-Creation-Date: 2016-01-26 17:45+0400\n"
"PO-Revision-Date: 2016-10-20 12:29+0400\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"X-Poedit-Basepath: ../frontend\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: views/WDPSViewSlider.php:32
#, fuzzy
msgid "There is no slider selected or the slider was deleted."
msgstr "Нема изабраних галерија или галерија је избрисан."

#: views/WDPSViewSlider.php:49
msgid "There are no posts in this slider."
msgstr "Нема порука у овом клизача."

#: views/WDPSViewSlider.php:1194
msgid "Share on Facebook"
msgstr "Подели на Фацебоок-у."

#: views/WDPSViewSlider.php:1200
msgid "Share on Twitter"
msgstr "Схаре он Твиттер"

#: views/WDPSViewSlider.php:1206
msgid "Share on Google+"
msgstr "Подели на Гоогле+"

#: views/WDPSViewSlider.php:1212
msgid "Share on Pinterest"
msgstr "Схаре он Pinterest"

#: views/WDPSViewSlider.php:1218
msgid "Share on Tumblr"
msgstr "Схаре он Tumblr"

#: views/WDPSViewSlider.php:2876 views/WDPSViewSlider.php:2949
msgid "Pause"
msgstr "Пауза"

#: views/WDPSViewSlider.php:2913
msgid "Play"
msgstr "Плаи"

#, fuzzy
#~ msgid "There are no slides in this slider."
#~ msgstr "Нема слика у овој галерији."

#~ msgid "field is required."
#~ msgstr "је обавезно поље. "

#~ msgid "This is not a valid email address."
#~ msgstr "Ово није валидна емаил адреса. "

#~ msgid "There are no images matching your search."
#~ msgstr "Нема слике одговарају претрагу."

#~ msgid " item(s)"
#~ msgstr "ставка(e)"

#~ msgid "First"
#~ msgstr "Прва"

#~ msgid "Previous"
#~ msgstr "Предходни"

#~ msgid "Next"
#~ msgstr "Даље"

#~ msgid "Last"
#~ msgstr "Ласт"

#~ msgid "Go to the first page"
#~ msgstr "Иди на првој страници"

#~ msgid "Go to the previous page"
#~ msgstr "Иди на претходну страницу"

#~ msgid "of"
#~ msgstr "од"

#~ msgid "Go to the next page"
#~ msgstr "Иди на следећу страницу"

#~ msgid "Go to the last page"
#~ msgstr "Иди на последњу страну"

#~ msgid "Reset"
#~ msgstr "Ресет"

#~ msgid "Search"
#~ msgstr "Тражи"

#~ msgid "There is no theme selected or the theme was deleted."
#~ msgstr "Нема изабраних тема или тема је избрисана."

#~ msgid "There is no album selected or the album was deleted."
#~ msgstr "Нема изабраних албума или албума је избрисан."

#~ msgid "Back"
#~ msgstr "Назад"

#~ msgid "Album is empty."
#~ msgstr "Албум је празан."

#~ msgid "Gallery is empty."
#~ msgstr "Галерија је празна."

#~ msgid "More"
#~ msgstr "Више"

#~ msgid "Hide"
#~ msgstr "Сакриј"

#~ msgid "Show comments"
#~ msgstr "Прикажи коментаре"

#~ msgid "The image has been deleted."
#~ msgstr "Слика је обрисан"

#~ msgid "Maximize"
#~ msgstr "Макимизе"

#~ msgid "Fullscreen"
#~ msgstr "Цео екран"

#~ msgid "Show info"
#~ msgstr "Схов инфо"

#~ msgid "Show rating"
#~ msgstr "Схов рејтинг"

#~ msgid "Open image in original size."
#~ msgstr "Отвори слику у оригиналној величини. "

#~ msgid "Download original image"
#~ msgstr "Довнлоад оригинал имаге"

#~ msgid "Hits: "
#~ msgstr "Хитс: "

#~ msgid "Rated."
#~ msgstr "Ратед."

#~ msgid "Not rated yet."
#~ msgstr "Још није оцењивано. "

#~ msgid "Votes: "
#~ msgstr "Гласова: "

#~ msgid "Cancel your rating."
#~ msgstr "Откажи свој рејтинг. "

#~ msgid "You have already rated."
#~ msgstr "Већ сте оценили. "

#~ msgid "Error. Incorrect Verification Code."
#~ msgstr "Грешка. Неправилно Верификациони код. "

#~ msgid "Hide Comments"
#~ msgstr "Сакриј коментаре"

#~ msgid "Name"
#~ msgstr "Име"

#~ msgid "Email"
#~ msgstr "Е-маил "

#~ msgid "Comment"
#~ msgstr "Коментар"

#~ msgid "Verification Code"
#~ msgstr "Верификациони код"

#~ msgid "Submit"
#~ msgstr "Субмит"

#~ msgid "Your comment is awaiting moderation"
#~ msgstr "Ваш коментар чека на модерацију "

#~ msgid "Delete Comment"
#~ msgstr "Обриши коментар"

#~ msgid "Show Comments"
#~ msgstr "Приказати коментаре"

#, fuzzy
#~ msgid "Hide info"
#~ msgstr "Сакриј"

#~ msgid "Hide rating"
#~ msgstr "Сакриј рејтинг"

#~ msgid "Restore"
#~ msgstr "Ресторе"

#~ msgid "Exit Fullscreen"
#~ msgstr "Екит Фуллсцреен"

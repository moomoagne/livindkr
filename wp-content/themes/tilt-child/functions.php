<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_style' );
function enqueue_parent_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function no_wordpress_errors(){
  return 'Nom utilisateur ou mot de passe incorrect';
}
/*add_filter( 'login_errors', 'no_wordpress_errors' );

function admin_default_page() {
  return '/';
}

add_filter('login_redirect', 'admin_default_page');*/
add_action( 'admin_menu', 'remove_dashboard', 99 );
function remove_dashboard(){
   remove_menu_page( 'index.php' ); //dashboard
}

function admin_default_page() {
   return 'wp-admin/edit.php?post_type=post'; //your redirect URL
}
add_filter('login_redirect', 'admin_default_page'); 
add_filter('widget_text', 'do_shortcode');
?>
